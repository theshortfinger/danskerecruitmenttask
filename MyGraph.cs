﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruitmentTask
{
    public class MyGraph
    {
        private Dictionary<int, Node> nodes = new Dictionary<int, Node>();
        //list of valid routes and sum of element values
        private List<Tuple<int, List<Node>>> validRoutes = new List<Tuple<int, List<Node>>>();
        public MyGraph()
        {
        }
        /// <summary>
        /// Method for inserting many values to our graph at once.
        /// Starts from the root, then goes from the bottom left to right.
        /// </summary>
        /// <param name="values"></param>
        public void InsertMany(List<int> values)
        {
            int level = 1;
            int hits = 0;
            int remaining = values.Count();

            for (int i = 0; i < values.Count();)
            {
                Node newNode = new Node(values[i]);
                if (!(remaining <= level))
                { 
                    newNode.id1 = i + level;
                    newNode.id2 = i + level + 1;
                }
                nodes.Add(i, newNode);
                hits++;
                if (hits == level)
                {
                    level++;
                    hits = 0;
                }
                remaining--;
                i++;
            }
        }
        public Tuple<int, List<Node>> FindRouteWithHighestValue()
        {
            if (nodes.Count() == 0)
                return null;
            else ContinueValidRoutes(new List<Node>() { nodes[0] });

            return validRoutes.OrderByDescending(x => x.Item1).Select(x => x).First();
        }
        private void ContinueValidRoutes(List<Node> route)
        {
            var root = route.Last();
            if (root.id1 == null && root.id2 == null)
            {
                validRoutes.Add(new Tuple<int, List<Node>>(route.Sum(x => x.value), route));
                return;
            }

            var validKids = GetValidKids(root);
            if (validKids.Count() == 0)
            {
                return;
            }
            else if (validKids.Count() == 1)
            {
                route.Add(validKids.FirstOrDefault());
                ContinueValidRoutes(route);
            }
            else if (validKids.Count() == 2)
            {
                List<Node> newRoute = new List<Node>();
                newRoute.AddRange(route);
                newRoute.Add(validKids.LastOrDefault());
                route.Add(validKids.FirstOrDefault());
                ContinueValidRoutes(route);
                ContinueValidRoutes(newRoute);
            }
        }
        private List<Node> GetValidKids(Node root)
        {
            var validKids = new List<Node>();
            if (root.id1 != null && IsValueEven(nodes[(int)root.id1].value) != IsValueEven(root.value))
            {
                validKids.Add(nodes[(int)root.id1]);
            }
            if (root.id2 != null && IsValueEven(nodes[(int)root.id2].value) != IsValueEven(root.value))
            {
                validKids.Add(nodes[(int)root.id2]);
            }
            return validKids;
        }
        private bool IsValueEven(int value)
        {
            return value % 2 == 0;
        }
    }
    public class Node
    {
        public Node(int value)
        {
            this.value = value;
        }
        public int value { get; set; }
        public int? id1 { get; set; }
        public int? id2 { get; set; }
    }
}
